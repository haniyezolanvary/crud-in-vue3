const DEFAULT_API_URL = 'http://localhost:3000'

export const baseURL = process.env.API_BASE_URL ?? DEFAULT_API_URL

