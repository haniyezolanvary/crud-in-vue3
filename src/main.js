import { createApp } from 'vue'
import App from './App.vue'
import router from './router/index'
import './assets/style/main.css'
import '@mdi/font/css/materialdesignicons.css'
import Toast from "vue-toastification";
import "vue-toastification/dist/index.css";
import vuetifyPlugin from './plugins/vuetify'
const app = createApp(App)


vuetifyPlugin(app)
// app.use(axios);
app.use(router).mount('#app')
app.use(Toast);
