import { createRouter, createWebHistory } from 'vue-router';
import Home from '../pages/Home.vue';

import TemplatePost from '../pages/posts/Template.vue';
import IndexPost from '../pages/posts/Index.vue';
import ShowPost from '../pages/posts/Show.vue';
import CreatePost from '../pages/posts/Create.vue';

const routes = [
    { path: '/', name: 'home', component: Home },
    {
        path: '/posts', name: 'postTemplate', component: TemplatePost, children: [
            { path: '', name: 'posts', component: IndexPost },
            { path: ':id', name: 'postId', component: ShowPost },
            { path: 'create', name: 'createPost', component: CreatePost },
        ],
    },
];

const router = createRouter({
    history: createWebHistory(),
    routes
});

export default router;