import axios from 'axios';
import { baseURL } from '../../configs';

const instance = axios.create({
    baseURL: baseURL // Read the base URL from the environment variable
});
export default instance;