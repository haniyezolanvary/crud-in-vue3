import 'vuetify/styles' // Make sure you have the correct path to the Vuetify styles
import { createVuetify } from 'vuetify'
import * as components from 'vuetify/lib/components' // Update the import path to include 'lib'
import * as directives from 'vuetify/lib/directives' // Update the import path to include 'lib'

export default function (app) {
    const vuetify = createVuetify({
        components,
        directives
    })

    app.use(vuetify)
}